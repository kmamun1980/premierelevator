angular.module("components", [])
	.directive("searchJobControl", function($http, $parse){
		
		return {
			restrict: "E",
			replace: true,
			scope: {
				job_number: "@",
				job: "=",
				selectJob: '='
			},
			templateUrl: "/statics/js/partials/search_job_control_form.html",
			
			compile: function(tElement, tAttrs, transclude) {

				// console.log(tAttrs.jobNumber);

				return function(scope, element, attrs) {
					
					scope.selectMyJob = function(job){
						scope.selectJob(job);
						scope.show_job_list = false;
						// scope.job_number_search = job.job_number;
						scope.job_number = job.job_number;
					}

					scope.searchJob = function(q) {
						
						if (q.length > 0) {
							var url = "/schedule/job-control/search/?q="+q+"/";
							$http({method: 'GET', url: url}).
				            success(function(data, status, headers, config) {
				                scope.jobs = data;
				                
				                // Select deafult contacts
				                if(data.length > 0) {
				                	scope.show_job_list = true;	
				                	scope.add_new_job = false;
				                	console.log(data);
				                } else {
				                	scope.show_job_list = false;
				                	scope.add_new_job = true;
				                }
				                
				            
				        	});
							
						} else {
							scope.show_job_list = false;
							scope.add_new_job = false;
						}
					}

					scope.closeJobList = function() {
						scope.show_job_list = false;
						scope.job_number = '';
					}
				}
			}
			
		}
	})
	.directive("searchContact", function($http){

		return {
			restrict: "E",
			replace: true,
			scope: {
				contactfieldname: "@",
				search_contact: "@",
				// contact: "=",
				setcontact: "&setcontact",
				setmycon: "&"
			},
			templateUrl: "/statics/js/partials/search_contact_form.html",
			
			compile: function(tElement, tAttrs, transclude) {

				// console.log(tElement);

				return function(scope, element, attrs) {
					// console.log(element);
					// console.log(element.html);
					scope.setmycon = function(contact) {
						// console.log(element);

						var con = contact;
						scope.show_contact_list = false;
						scope.search_contact = contact.contact_name;
						var funcname = scope.setcontact();
						$(element).click(function(e){
							funcname(con);
						});
						
					}
					scope.hideContactList = function() {
						scope.show_contact_list = false;
						scope.search_contact = '';
					}

					scope.searchContact = function(q) {
						// console.log(q);
						if(q != undefined){
							if(q.length > 1 ) {
					    		var url = "/contacts/search/?q=" + q
					    		
					    		$http({method: 'GET', url: url}).
						            success(function(data, status, headers, config) {
						            	if(data.length > 0) {
						            		scope.contacts = data;
						            		scope.show_contact_list = true;	
						            	} else {
						            		scope.show_contact_list = false;
						            	}
						            });					        
					    		
					    	} else {
					    		scope.show_contact_list = false;
					    	}
						} else {
							scope.show_contact_list = false;
						}
					}
				}	
			}
		}
	})
	.directive("searchItem", function($http){

		return {
			restrict: "E",
			replace: true,
			scope: {
				search_item: "@",
				item: "=",
				selectItem: '=',
				addService: '='
			},

			templateUrl: "/statics/js/partials/search_item_form.html",
			
			compile: function(tElement, tAttrs, transclude) {

				return function(scope, element, attrs) {
					
					scope.closeMe = function() {
						scope.show_item_list = false;
						// scope.search_item = '';
					}

					scope.selectMyItem = function(item){
						scope.selectItem(item);
						scope.show_item_list = false;
						
					}
					
					scope.searchItem = function(q) {
						// console.log(q);
						if(q != undefined){
							if(q.length > 1 ) {
					    		var url = "/inventory/search/?q=" + q
					    		
					    		$http({method: 'GET', url: url}).
						            success(function(data, status, headers, config) {
						            	if(data.length > 0) {
						            		console.log(data);
						            		scope.items = data;
						            		scope.show_item_list = true;	
						            	} else {
						            		scope.show_item_list = false;
						            	}
						            });					        
					    		
					    	} else if (q.length == 1 && q == '.') {
					    		
					    		
					    			scope.addService();
					    		
					    	} else {
					    		scope.show_item_list = false;
					    	}
						} else {
							scope.show_item_list = false;
						}
					}
				}	
			}
		}
	})