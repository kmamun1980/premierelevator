from django import forms
from purchase.models import *
from purchase.choices import *
from contacts.models import *
from premierelevator.models import SystemVariable
import datetime
import re


class CustomModelChoiceField(forms.ModelChoiceField):
     def label_from_instance(self, obj):
         return "%s %s" % (obj.first_name, obj.last_name)

class InvoiceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.action = kwargs.pop('action', None)
        super(InvoiceForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Invoice
        exclude = ["invoice_number",]

    def save(self, commit=True):
        invoice = super(InvoiceForm, self).save(commit=False)

        if self.action == 'new':
            sv = SystemVariable.objects.get(id=1)
            next_invoice_number = sv.get_next_invoice_number
            # invoice.invoiced_by = self.request.user
            invoice.status = 0
            invoice.invoice_number = next_invoice_number
            sv.save()
        invoice.save()
        return invoice

class PRAddForm(forms.ModelForm):
    user_requested = CustomModelChoiceField(required=False, queryset=User.objects.filter(id__gte=0), 
        widget=forms.Select(attrs={"class": "form-control"}))
    item = forms.ModelChoiceField(required=False, queryset=Item.objects.all(), 
        widget=forms.Select(attrs={"class": "form-control"}))
    description = forms.CharField(required=False,
        widget=forms.Textarea(attrs={"class": "form-control", "rows":3, 'placeholder':"Describe why needed.."}))
    order_qty = forms.DecimalField( max_digits=10, required=False, decimal_places=2,
        widget=forms.NumberInput(attrs={"class": "form-control decimal", 
            'placeholder': "qty"}))
    item_require_before = forms.DateField(required=False, widget=forms.DateInput(
        attrs={"class": "form-control datepicker", 'placeholder':"Item Require Before",}))

    class Meta:
        """docstring for Meta"""
        model = PurchaseRequest
        exclude = ['approved_qty', 'requeste_created_at', 'status', 'approved_date']

class PRApproveForm(forms.ModelForm):
    approved_qty = forms.DecimalField( max_digits=10, required=False, decimal_places=2,
        widget=forms.NumberInput(attrs={"class": "form-control decimal", 'placeholder': "qty"}))
    
    class Meta:
        """docstring for Meta"""
        model = PurchaseRequest
        exclude = ['user_requested', 'status', 'item', 'description', 'order_qty', 'item_require_before',  'requeste_created_at', 'approved_date']

class PRCAddForm(forms.ModelForm):
    comment = forms.CharField(required=True,
        widget=forms.Textarea(attrs={"class": "form-control", "rows":3}))

    class Meta:
        """docstring for Meta"""
        model = PurchaseRequestComment
        exclude = ['purchase_request', 'commnet_date', 'user_commented']


class POForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.action = kwargs.pop('action', None)
        super(POForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        po = super(POForm, self).save(commit=False)
        if self.action == 'new':
            sv = SystemVariable.objects.get(id=1)
            next_po_number = sv.get_next_po_number
            po.po_created_by = self.request.user
            po.datetime = datetime.datetime.now()
            po.po_number = next_po_number
        elif self.action == 'update':
            po.po_created_by = self.request.user
            po.datetime = datetime.datetime.now()
        po.save()
        return po
    


    class Meta:
        """docstring for Meta"""
        model = PurchaseOrder
        exclude = ['po_number', 'po_created_by', 'search_string', 'datetime']


class POFormEdit(POForm):
    po_status = forms.ChoiceField(required=False, choices=PO_STATUS,
        widget=forms.Select(attrs={"class": "form-control"}))


class POStatusForm(forms.ModelForm):
    status_comment = forms.CharField(max_length=1000, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", "rows":2, 'placeholder':"Put Status Comments"}))

    class Meta:
        model = POStatus
        exclude = ['status', 'po', 'datetime', 'status_by']


class POStatusForm2(forms.ModelForm):
    po = forms.ModelChoiceField(required=False, queryset=PurchaseOrder.objects.all(), 
        widget=forms.Select(attrs={"class": "form-control"}))
    status = forms.ChoiceField(required=False, choices=PO_STATUS,
        widget=forms.Select(attrs={"class": "form-control"}))
    status_by = CustomModelChoiceField(required=False, queryset=User.objects.filter(id__gte=0), 
        widget=forms.Select(attrs={"class": "form-control", 'disabled':True}))
    status_comment = forms.CharField(max_length=1000, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", "rows":2, 'placeholder':"Put Status Comments"}))

    class Meta:
        model = POStatus
        exclude = ['datetime',]


class POSupplierContactForm(forms.ModelForm):
    
    class Meta:
        model = POSupplierContact
        exclude = ['po',]


class POShipToContactForm(forms.ModelForm):

    class Meta:
        model = POShipToContact
        exclude = ['po',]


class SLShipToContactForm(forms.ModelForm):
    contact_type = forms.ChoiceField(required=False, choices=CON_TYPE,
        widget=forms.Select(attrs={"class": "form-control"}))
    contact = forms.CharField( max_length=100, required=False, label='Enter Email',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "type contact"}))
    contact_name = forms.CharField( max_length=100, required=False, label='Contact Name',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "contact name"}))
    
    class Meta:
        model = SLShipToContact
        exclude = ['sl',]


class SLSoldToContactForm(forms.ModelForm):
    contact_type = forms.ChoiceField(required=False, choices=CON_TYPE,
        widget=forms.Select(attrs={"class": "form-control"}))
    contact = forms.CharField( max_length=100, required=False, label='Enter Email',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "type contact"}))
    contact_name = forms.CharField( max_length=100, required=False, label='Contact Name',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "contact name"}))
    
    class Meta:
        model = SLSoldToContact
        exclude = ['sl',]

# PL Form
class PLSoldToContactForm(forms.ModelForm):
    contact_type = forms.ChoiceField(required=False, choices=CON_TYPE,
        widget=forms.Select(attrs={"class": "form-control"}))
    contact = forms.CharField( max_length=100, required=False, label='Enter Email',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "type contact"}))
    contact_name = forms.CharField( max_length=100, required=False, label='Contact Name',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "contact name"}))
    
    class Meta:
        model = PLSoldToContact
        exclude = ['pl',]

class PLShipToContactForm(forms.ModelForm):
    contact_type = forms.ChoiceField(required=False, choices=CON_TYPE,
        widget=forms.Select(attrs={"class": "form-control"}))
    contact = forms.CharField( max_length=100, required=False, label='Enter Email',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "type contact"}))
    contact_name = forms.CharField( max_length=100, required=False, label='Contact Name',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "contact name"}))
    
    class Meta:
        model = PLShipToContact
        exclude = ['pl',]

class PLCBContactForm(forms.ModelForm):
    contact_type = forms.ChoiceField(required=False, choices=CON_TYPE,
        widget=forms.Select(attrs={"class": "form-control"}))
    contact = forms.CharField( max_length=100, required=False, label='Enter Email',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "type contact"}))
    contact_name = forms.CharField( max_length=100, required=False, label='Contact Name',
        widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': "contact name"}))
    
    class Meta:
        model = PLCBContact
        exclude = ['pl',]

class ItemReeiveForm(forms.ModelForm):
    # purchase_item = forms.ModelChoiceField(required=False, queryset=PurchaseItem.objects.all(), 
    #   widget=forms.Select(attrs={"class": "form-control", 'readonly':'readonly'}))
    # item_po = forms.ModelChoiceField(required=False, queryset=PurchaseOrder.objects.all(), 
    #   widget=forms.Select(attrs={"class": "form-control"}))
    qty_received = forms.DecimalField( max_digits=10, required=False, decimal_places=2,
        widget=forms.NumberInput(attrs={"class": "form-control decimal", "ng-model":"qty",
            'placeholder': "qty received", 'max':"{{order_restriction}}"}))
    comment = forms.CharField(max_length=1000, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", "rows":2, 'placeholder':"Comments"}))

    class Meta:
        model = ReceivedItemHistory
        exclude = ['item_received_date', 'status', 'sub_total', 'reveived_by', 'search_string', 'purchase_item', 'item_po']

class PackingListForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.action = kwargs.pop('action', None)
        super(PackingListForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        # import pdb; pdb.set_trace();
        pl = super(PackingListForm, self).save(commit=False)
        pl.save()
        return pl

    class Meta:
        model = PackingList

class ShippingListForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.action = kwargs.pop('action', None)
        super(ShippingListForm, self).__init__(*args, **kwargs)

    class Meta:
        model = ShippingList
        

    def save(self, commit=True):
        sl = super(ShippingListForm, self).save(commit=False)
        
        sl.save()
        return sl






