from haystack import routers


class SLRouter(routers.BaseRouter):
    def for_write(self, **hints):
        return 'sl'

    def for_read(self, **hints):
        return 'sl'

class SLItemRouter(routers.BaseRouter):
    def for_write(self, **hints):
        return 'slitem'

    def for_read(self, **hints):
        return 'slitem'

class PLRouter(routers.BaseRouter):
    def for_write(self, **hints):
        return 'pl'

    def for_read(self, **hints):
        return 'pl'


class PORouter(routers.BaseRouter):
    def for_write(self, **hints):
        return 'po'

    def for_read(self, **hints):
        return 'po'


class InvoiceRouter(routers.BaseRouter):
    def for_write(self, **hints):
        return 'invoice'

    def for_read(self, **hints):
        return 'invoice'