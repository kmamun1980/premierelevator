import datetime
from haystack import indexes
from purchase.models import *

class PurchaseOrderIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    po_number = indexes.CharField(model_attr='po_number', boost=2)
    date_issued = indexes.DateField(model_attr='date_issued', indexed=False, null=True)
    date_expected = indexes.DateField(model_attr='date_expected', indexed=False, null=True)
    fob = indexes.CharField(model_attr='fob', null=True)
    shipping_inst = indexes.CharField(model_attr='shipping_inst', null=True)
    date_confirmed = indexes.DateField(model_attr='date_confirmed', indexed=False, null=True)
    blanket_po = indexes.CharField(model_attr='blanket_po', null=True)
    # returned_type = indexes.CharField(model_attr='returned_type', null=True)
    items_total = indexes.DecimalField(model_attr='items_total', default=0.0)
    po_total_before_tax = indexes.DecimalField(model_attr='po_total_before_tax', default=0.0)

    hst_taxable = indexes.BooleanField(model_attr='hst_taxable', null=True)
    hst_taxable_amount = indexes.DecimalField(model_attr='hst_taxable_amount', default=0.0)
    total_hst_tax = indexes.DecimalField(model_attr='total_hst_tax', default=0.0)
    
    pst_taxable = indexes.BooleanField(model_attr='pst_taxable', null=True)
    pst_taxable_amount = indexes.DecimalField(model_attr='pst_taxable_amount', default=0.0)
    total_pst_tax = indexes.DecimalField(model_attr='total_pst_tax', default=0.0)

    total_tax = indexes.DecimalField(model_attr='total_tax', default=0.0)
    total_po_amount = indexes.DecimalField(model_attr='total_po_amount', default=0.0)    
    datetime = indexes.DateTimeField(model_attr='datetime', null=True)
    po_que = indexes.CharField(model_attr='po_que', null=True)
    
    
    pdf_po = indexes.BooleanField(model_attr='pdf_po', null=True)
    print_po = indexes.BooleanField(model_attr='print_po', null=True)
    email_po = indexes.BooleanField(model_attr='email_po', null=True)
    fax_po = indexes.BooleanField(model_attr='fax_po', null=True)


    def get_model(self):
        return PurchaseOrder

    def prepare(self, obj):
        self.prepared_data = super(PurchaseOrderIndex, self).prepare(obj)
        supplier = obj.supplier
        ship_to = obj.ship_to
        
        if supplier:
            supplier_dict = {}
            supplier_dict['id'] = supplier.id
            supplier_dict['contact_name'] = supplier.contact_name
            supplier_dict['attention_to'] = supplier.attention_to
            supplier_dict['address_1'] = supplier.address_1
            supplier_dict['address_2'] = supplier.address_2
            supplier_dict['city'] = supplier.city
            supplier_dict['province'] = supplier.province
            supplier_dict['country'] = supplier.country
            supplier_dict['postal_code'] = supplier.postal_code
            
            phones = POSupplierContact.objects.filter(po=obj, contact_type='phone')
            phs = []
            for ph in phones:
                phone = {}
                phone['id'] = ph.id
                phone['department'] = ph.department
                phone['number'] = ph.contact
                phone['ext'] = ph.contact_ext
                phone['contact_name'] = ph.contact_name
                phs.append(phone)
            supplier_dict['phones'] = phs
            
            emails = POSupplierContact.objects.filter(po=obj, contact_type='email')
            elist = []
            for em in emails:
                email = {}
                email['id'] = em.id
                email['department'] = em.department
                email['email'] = em.contact
                email['contact_name'] = em.contact_name
                elist.append(email)
            supplier_dict['emails'] = elist

            faxes = POSupplierContact.objects.filter(po=obj, contact_type='fax')
            fax_list = []
            for fx in faxes:
                fax = {}
                fax['id'] = fx.id
                fax['department'] = fx.department
                fax['number'] = fx.contact
                fax['ext'] = fx.contact_ext
                fax['contact_name'] = fx.contact_name
                fax_list.append(fax)
            supplier_dict['faxes'] = fax_list

            self.prepared_data['supplier'] = supplier_dict
        else:
            self.prepared_data['supplier'] = None

        if ship_to:
            shipto_dict = {}
            shipto_dict['id'] = ship_to.id
            shipto_dict['contact_name'] = ship_to.contact_name
            shipto_dict['attention_to'] = ship_to.attention_to
            shipto_dict['address_1'] = ship_to.address_1
            shipto_dict['address_2'] = ship_to.address_2
            shipto_dict['city'] = ship_to.city
            shipto_dict['province'] = ship_to.province
            shipto_dict['country'] = ship_to.country
            shipto_dict['postal_code'] = ship_to.postal_code
            
            phones = POShipToContact.objects.filter(po=obj, contact_type='phone')
            phs = []
            for ph in phones:
                phone = {}
                phone['id'] = ph.id
                phone['department'] = ph.department
                phone['number'] = ph.contact
                phone['ext'] = ph.contact_ext
                phone['contact_name'] = ph.contact_name
                phs.append(phone)
            shipto_dict['phones'] = phs
            
            emails = POShipToContact.objects.filter(po=obj, contact_type='email')
            elist = []
            for em in emails:
                email = {}
                email['id'] = em.id
                email['department'] = em.department
                email['email'] = em.contact
                email['contact_name'] = em.contact_name
                elist.append(email)
            shipto_dict['emails'] = elist

            faxes = POShipToContact.objects.filter(po=obj, contact_type='fax')
            fax_list = []
            for fx in faxes:
                fax = {}
                fax['id'] = fx.id
                fax['department'] = fx.department
                fax['number'] = fx.contact
                fax['ext'] = fx.contact_ext
                fax['contact_name'] = fx.contact_name
                fax_list.append(fax)
            shipto_dict['faxes'] = fax_list

            self.prepared_data['ship_to'] = shipto_dict
        else:
            self.prepared_data['ship_to'] = None

        if obj.ship_via:
            ship = {}
            ship['id'] = obj.ship_via.id
            ship['delivery_choice'] = obj.ship_via.delivery_choice
            self.prepared_data['ship_via'] = ship
        else:
            self.prepared_data['ship_via'] = None

        if obj.terms:
            term = {}
            term['id'] = obj.terms.id
            term['term'] = obj.terms.term
            self.prepared_data['terms'] = term
        else:
            self.prepared_data['terms'] = None

        if obj.deliver_internal:
            deliver_internal_dict = {}
            deliver_internal_dict['id'] = obj.deliver_internal.id
            deliver_internal_dict['department'] = obj.deliver_internal.department
            deliver_internal_dict['description'] = obj.deliver_internal.description            
            self.prepared_data['deliver_internal'] = deliver_internal_dict
        else:
            self.prepared_data['deliver_internal'] = None

        if obj.purchasing_agent:
            order_by = {}
            order_by['id'] = obj.purchasing_agent.id
            order_by['username'] = obj.purchasing_agent.username
            order_by['email'] = obj.purchasing_agent.email
            self.prepared_data['purchasing_agent'] = order_by
        else:
            self.prepared_data['purchasing_agent'] = None

        if obj.returned_type:
            returntype = {}
            returntype['id'] = obj.returned_type
            returntype['type'] = obj.get_return_type()
            self.prepared_data['returned_type'] = returntype
        else:
            self.prepared_data['returned_type'] = None

        if obj.po_currency:
            currency = {}
            currency['id'] = obj.po_currency.id
            currency['currency'] = obj.po_currency.currency
            self.prepared_data['currency'] = currency
        else:
            self.prepared_data['currency'] = None
      
        items = PurchaseItem.objects.filter(po=obj).order_by("line_item_seq");
        item_list = []
        
        for item in items:
            item_dic = {}
            item_dic['id'] = item.id
            item_dic['line_item_seq'] = item.line_item_seq
            if item.item:
                item_dic['item_number'] = item.item.item_number
            else:
                item_dic['item_number'] = '.'
            item_dic['item_description'] = item.item_description
            
            if item.job_number:
                sold_to = {}
                if item.job_number.sold_to:
                    sold_to['contact_name'] = item.job_number.sold_to.contact_name
                    sold_to['attention_to'] = item.job_number.sold_to.attention_to

                job_control = {}
                
                job_control['job_number'] = item.job_number.job_number
                job_control['job_name'] = item.job_number.job_name
                job_control['sold_to'] = sold_to

                item_dic['job_number'] = job_control
            else:
                item_dic['job_number'] = None

            if item.unit:
                unit = {}
                unit['id'] = item.unit.id
                unit['unit_name'] = item.unit.unit_name
                item_dic['unit'] = unit
            else:
                item_dic['unit'] = None

            item_dic['qty_ordered'] = item.qty
            item_dic['cost'] = item.cost
            item_dic['sub_total'] = item.sub_total
            item_dic['item_recv'] = item.item_recv
            item_dic['item_recv_date'] = item.item_recv_date
            item_dic['custom_detail'] = item.custom_detail
            
            status = {}
            status['id'] = item.purchase_status
            status['status'] = item.status_verbose()
            item_dic['purchase_status'] = status

            item_list.append(item_dic)

        self.prepared_data['po_items'] = item_list


        status = {}
        status['id'] = obj.po_status
        status['status'] = obj.status_verbose()

        self.prepared_data['po_status'] = status
        
        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

class SLItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    item_number = indexes.CharField(model_attr='item', null=True)
    description = indexes.CharField(model_attr='description', null=True)
    ordered = indexes.CharField(model_attr='ordered', null=True)
    shipped_total_to_date = indexes.CharField(model_attr='shipped_total_to_date', null=True)
    backordered = indexes.CharField(model_attr='backordered', null=True)
    shipping_list = indexes.CharField(model_attr='shipping_list', null=True)
    search_string = indexes.CharField(model_attr='search_string', null=True)

    def get_model(self):
        return ShippingItem

class ShippingListIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    sl_number = indexes.CharField(model_attr='sl_number', boost=2)
    date_required = indexes.CharField(model_attr='date_required', indexed=False, null=True)
    ordered_date = indexes.DateField(model_attr='ordered_date', indexed=False, null=True)
    pdf_sl = indexes.BooleanField(model_attr='pdf_sl', null=True)
    print_sl = indexes.BooleanField(model_attr='print_sl', null=True)
    email_sl = indexes.BooleanField(model_attr='email_sl', null=True)
    fax_sl = indexes.BooleanField(model_attr='fax_sl', null=True)

    def get_model(self):
        return ShippingList

    def prepare(self, obj):
        self.prepared_data = super(ShippingListIndex, self).prepare(obj)
        sold_to = obj.sold_to

        items = ShippingItem.objects.filter(shipping_list=obj).order_by("sl_item_seq")

        slitems = []
        for item in items:
            item_dict = {}
            item_dict['id'] = item.id
            item_dict['item_number'] = item.item.item_number
            item_dict['sl_item_seq'] = item.sl_item_seq
            item_dict['description'] = item.description
            item_dict['ordered'] = item.ordered
            item_dict['shipped_total_to_date'] = item.shipped_total_to_date
            item_dict['backordered'] = item.backordered
            
            if item.unit_measure:
                unit = {}
                unit['id'] = item.unit_measure.id
                unit['unit_name'] = item.unit_measure.unit_name
                item_dict['unit'] = unit
            else:
                item_dict['unit'] = None

            item_dict['comment'] = item.search_string

            slitems.append(item_dict)

        self.prepared_data['sl_items'] = slitems

        
        if sold_to:
            sold_to_dict = {}
            sold_to_dict['id'] = sold_to.id
            sold_to_dict['contact_name'] = sold_to.contact_name
            sold_to_dict['attention_to'] = sold_to.attention_to
            sold_to_dict['address_1'] = sold_to.address_1
            sold_to_dict['address_2'] = sold_to.address_2
            sold_to_dict['city'] = sold_to.city
            sold_to_dict['province'] = sold_to.province
            sold_to_dict['country'] = sold_to.country
            sold_to_dict['postal_code'] = sold_to.postal_code
            
            phones = SLSoldToContact.objects.filter(sl=obj, contact_type='phone')
            phs = []
            for ph in phones:
                phone = {}
                phone['id'] = ph.id
                phone['department'] = ph.department
                phone['number'] = ph.contact
                phone['ext'] = ph.contact_ext
                phone['contact_name'] = ph.contact_name
                phs.append(phone)
            sold_to_dict['phones'] = phs
            
            emails = SLSoldToContact.objects.filter(sl=obj, contact_type='email')
            elist = []
            for em in emails:
                email = {}
                email['id'] = em.id
                email['department'] = em.department
                email['email'] = em.contact
                email['contact_name'] = em.contact_name
                elist.append(email)
            sold_to_dict['emails'] = elist

            faxes = SLSoldToContact.objects.filter(sl=obj, contact_type='fax')
            fax_list = []
            for fx in faxes:
                fax = {}
                fax['id'] = fx.id
                fax['department'] = fx.department
                fax['number'] = fx.contact
                fax['ext'] = fx.contact_ext
                fax['contact_name'] = fx.contact_name
                fax_list.append(fax)
            sold_to_dict['faxes'] = fax_list

            self.prepared_data['sold_to'] = sold_to_dict
        else:
            self.prepared_data['sold_to'] = None

        # ============= Ship To =================
        ship_to = obj.ship_to

        if ship_to:
            shipto_dict = {}
            shipto_dict['id'] = ship_to.id
            shipto_dict['contact_name'] = ship_to.contact_name
            shipto_dict['attention_to'] = ship_to.attention_to
            shipto_dict['address_1'] = ship_to.address_1
            shipto_dict['address_2'] = ship_to.address_2
            shipto_dict['city'] = ship_to.city
            shipto_dict['province'] = ship_to.province
            shipto_dict['country'] = ship_to.country
            shipto_dict['postal_code'] = ship_to.postal_code
            
            phones = SLShipToContact.objects.filter(sl=obj, contact_type='phone')
            phs = []
            for ph in phones:
                phone = {}
                phone['id'] = ph.id
                phone['department'] = ph.department
                phone['number'] = ph.contact
                phone['ext'] = ph.contact_ext
                phone['contact_name'] = ph.contact_name
                phs.append(phone)
            shipto_dict['phones'] = phs
            
            emails = SLShipToContact.objects.filter(sl=obj, contact_type='email')
            elist = []
            for em in emails:
                email = {}
                email['id'] = em.id
                email['department'] = em.department
                email['email'] = em.contact
                email['contact_name'] = em.contact_name
                elist.append(email)
            shipto_dict['emails'] = elist

            faxes = SLShipToContact.objects.filter(sl=obj, contact_type='fax')
            fax_list = []
            for fx in faxes:
                fax = {}
                fax['id'] = fx.id
                fax['department'] = fx.department
                fax['number'] = fx.contact
                fax['ext'] = fx.contact_ext
                fax['contact_name'] = fx.contact_name
                fax_list.append(fax)
            shipto_dict['faxes'] = fax_list

            self.prepared_data['ship_to'] = shipto_dict
        else:
            self.prepared_data['ship_to'] = None


        self.prepared_data['status'] = obj.status_verbose()
        self.prepared_data['status_id'] = obj.sl_status



        if obj.job_number:
            sold_to = {}
            if obj.job_number.sold_to:
                sold_to['contact_name'] = obj.job_number.sold_to.contact_name
                sold_to['attention_to'] = obj.job_number.sold_to.attention_to

            job_control = {}
            
            job_control['job_number'] = obj.job_number.job_number
            job_control['job_name'] = obj.job_number.job_name
            job_control['sold_to'] = sold_to

            self.prepared_data['job'] = job_control
        
        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

class PackingListIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    pl_number = indexes.CharField(model_attr='pl_number', boost=2)
    date_issued = indexes.CharField(model_attr='date_issued', indexed=False, null=True)
    shipping_bl_number = indexes.CharField(model_attr='shipping_bl_number', indexed=False, null=True)
    invoiced_on_date = indexes.DateField(model_attr='invoiced_on_date', indexed=False, null=True)
    pdf_pl = indexes.BooleanField(model_attr='pdf_pl', null=True)
    print_pl = indexes.BooleanField(model_attr='print_pl', null=True)
    email_pl = indexes.BooleanField(model_attr='email_pl', null=True)
    fax_pl = indexes.BooleanField(model_attr='fax_pl', null=True)

    def get_model(self):
        return PackingList

    def prepare(self, obj):
        self.prepared_data = super(PackingListIndex, self).prepare(obj)
        sold_to = obj.sold_to

        pl_items = PackingItem.objects.filter(pl=obj)
        items = []
        for item in pl_items:
            item_dict = {}
            item_dict['id'] = item.id
            if item.sl_item:
                item_dict['sl_item'] = item.sl_item
            else:
                item_dict['sl_item'] = None

            if item.item:
                item_dict['item_number'] = item.item.item_number
            else:
                item_dict['item_number'] = None

            item_dict['pl_item_seq'] = item.pl_item_seq
            item_dict['description'] = item.description
            if item.unit:
                unit = {}
                unit['id'] = item.unit.id
                unit['unit_name'] = item.unit.unit_name
                item_dict['unit'] = unit
            else:
                item_dict['unit'] = None
            item_dict['qty_ordered'] = item.qty_ordered
            item_dict['pl'] = item.pl
            item_dict['line_comments'] = item.line_comments


        if obj.sold_to:
            sold_to_dict = {}
            sold_to_dict['contact_name'] = sold_to.contact_name
            phones = sold_to.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            sold_to_dict['phones'] = phs
            emails = sold_to.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            sold_to_dict['emails'] = elist

            self.prepared_data['sold_to'] = sold_to_dict
        # ============= Ship To =================
        if obj.ship_to:
            ship_to = obj.ship_to
            ship_to_dict = {}
            ship_to_dict['contact_name'] = ship_to.contact_name
            phones = ship_to.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            ship_to_dict['phones'] = phs
            emails = ship_to.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            ship_to_dict['emails'] = elist

            self.prepared_data['ship_to'] = ship_to_dict
        # self.prepared_data['status'] = obj.status_verbose()
        self.prepared_data['date_shipped'] = obj.date_shipped
        status = {}
        if obj.status:
            status['id'] = obj.status
            status['status'] = obj.status_verbose()
        self.prepared_data['status'] = status
        
        if obj.job_number:
            sold_to = {}
            if obj.job_number.sold_to:
                sold_to['contact_name'] = obj.job_number.sold_to.contact_name
                sold_to['attention_to'] = obj.job_number.sold_to.attention_to

            job_control = {}
            
            job_control['job_number'] = obj.job_number.job_number
            job_control['job_name'] = obj.job_number.job_name
            job_control['sold_to'] = sold_to

            self.prepared_data['job'] = job_control
        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

class InvoiceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    invoice_number = indexes.CharField(model_attr='invoice_number', boost=2)
    invoice_date = indexes.DateField(model_attr='date', indexed=False, null=True)
    fob = indexes.CharField(model_attr='fob', indexed=False, null=True)
    po = indexes.CharField(model_attr='po', indexed=False, null=True)
    invoice_qty = indexes.DecimalField(model_attr='invoice_qty', indexed=False, null=True)
    sub_total = indexes.DecimalField(model_attr='sub_total', indexed=False, null=True)
    discount = indexes.DecimalField(model_attr='discount', indexed=False, null=True)
    discount_type = indexes.CharField(model_attr='discount_type', indexed=False, null=True)
    discounted_sub_total = indexes.DecimalField(model_attr='discounted_sub_total', indexed=False, null=True)
    comment = indexes.CharField(model_attr='comment', indexed=False, null=True)
    hst_taxable = indexes.BooleanField(model_attr='hst_taxable', indexed=False, null=True)
    hst_taxable_amount = indexes.DecimalField(model_attr='hst_taxable_amount', indexed=False, null=True)
    pst_taxable = indexes.BooleanField(model_attr='pst_taxable', indexed=False, null=True)
    pst_taxable_amount = indexes.DecimalField(model_attr='pst_taxable_amount', indexed=False, null=True)
    total_amount = indexes.DecimalField(model_attr='total_amount', indexed=False, null=True)

    pdf_invoice = indexes.BooleanField(model_attr='pdf_invoice', null=True)
    print_invoice = indexes.BooleanField(model_attr='print_invoice', null=True)
    email_invoice = indexes.BooleanField(model_attr='email_invoice', null=True)
    fax_invoice = indexes.BooleanField(model_attr='fax_invoice', null=True)

    def get_model(self):
        return Invoice

    def prepare(self, obj):
        self.prepared_data = super(InvoiceIndex, self).prepare(obj)
        sold_to = obj.sold_to

        items = InvoicedItem.objects.filter(invoice=obj)
        invoice_items = []

        for it in items:
            item = {}
            item['id'] = it.id
            item['invoice_item_seq'] = it.invoice_item_seq
            item['description'] = it.description

            if it.plitem:
                item['plitem_id'] = it.plitem.id

            if it.item:
                item['item_number'] = it.item.item_number

            if it.unit:
                unit = {}
                unit['id'] = it.unit.id
                unit['unit_name'] = it.unit.unit_name
                item['unit'] = unit

            item['qty'] = it.qty
            item['price'] = it.price
            item['sub_total'] = it.sub_total

            invoice_items.append(item)

        self.prepared_data['invoice_items'] = invoice_items

        self.prepared_data['status'] = obj.status_verbose
        self.prepared_data['status_id'] = obj.status

        if obj.invoice_currency:
            currency = {}
            currency['id'] = obj.invoice_currency.id
            currency['currency'] = obj.invoice_currency.currency
            currency['currency_icon'] = obj.invoice_currency.get_currency_icon()
            self.prepared_data['currency'] = currency
        else:
            self.prepared_data['currency'] = None

        if obj.terms:
            term = {}
            term['id'] = obj.terms.id
            term['term'] = obj.terms.term
            self.prepared_data['term'] = term

        if obj.ship_via:
            shipvia = {}
            shipvia['id'] = obj.ship_via.id
            shipvia['delivery_choice'] = obj.ship_via.delivery_choice
            self.prepared_data['ship_via'] = shipvia

        if obj.sold_to:
            sold_to_dict = {}
            sold_to_dict['contact_name'] = sold_to.contact_name
            phones = sold_to.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            sold_to_dict['phones'] = phs
            emails = sold_to.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            sold_to_dict['emails'] = elist

            self.prepared_data['sold_to'] = sold_to_dict
        # ============= Ship To =================
        if obj.ship_to:
            ship_to = obj.ship_to
            ship_to_dict = {}
            ship_to_dict['contact_name'] = ship_to.contact_name
            phones = ship_to.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            ship_to_dict['phones'] = phs
            emails = ship_to.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            ship_to_dict['emails'] = elist

            self.prepared_data['ship_to'] = ship_to_dict

        # ================ Broker ======================
        if obj.broker:
            broker = obj.broker
            broker_dict = {}
            broker_dict['contact_name'] = broker.contact_name
            broker_dict['attention_to'] = broker.attention_to

            phones = broker.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            broker_dict['phones'] = phs

            emails = broker.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            broker_dict['emails'] = elist

            self.prepared_data['broker'] = broker_dict

        packing_list = {}
        if obj.pl:
            pl = obj.pl
            packing_list['pl_number'] = pl.pl_number
            self.prepared_data['pl'] = packing_list
            

        user = {}
        if obj.invoiced_by:            
            user['id'] = obj.invoiced_by.id
            user['username'] = obj.invoiced_by.username
            user['email'] = obj.invoiced_by.email

        self.prepared_data['invoiced_by'] = user

        if obj.job_number:
            sold_to = {}
            if obj.job_number.sold_to:
                sold_to['contact_name'] = obj.job_number.sold_to.contact_name
                sold_to['attention_to'] = obj.job_number.sold_to.attention_to

            job_control = {}
            
            job_control['job_number'] = obj.job_number.job_number
            job_control['job_name'] = obj.job_number.job_name
            job_control['sold_to'] = sold_to

            self.prepared_data['job'] = job_control
        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


