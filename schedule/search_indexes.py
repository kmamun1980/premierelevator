import datetime
from haystack import indexes
from schedule.models import Job, JobControl, JobTimeControl
from contacts.models import ContactPhone, ContactEmailAddress


class JobIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    job_number = indexes.CharField(model_attr='job_number', boost=2)
    cab_designation = indexes.CharField(model_attr='cab_designation', indexed=False, null=True)
    job_name = indexes.CharField(model_attr='job_name', indexed=False, null=True)
    customer_contact_name = indexes.CharField(model_attr='customer_contact_name', indexed=False,  null=True)
    address_1 = indexes.CharField(model_attr='address_1', indexed=False, null=True)
    customer_contact_phone_number = indexes.CharField(model_attr='customer_contact_phone_number', indexed=False,  null=True)
    po_number = indexes.CharField(model_attr='po_number', indexed=False,  null=True)
    
    # phones = indexes.MultiValueField(indexed=False, null=True)
    # search_string = indexes.NgramField(model_attr='search_string')

    def get_model(self):
        return Job

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

class JobControlIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    job_number = indexes.CharField(model_attr='job_number', boost=2)
    job_name = indexes.CharField(model_attr='job_name', boost=2, null=True)
    elevetor_type = indexes.CharField(model_attr='elevetor_type__elevetor_type', indexed=False, null=True)
    elevetor_type_id = indexes.CharField(model_attr='elevetor_type__id', indexed=False, null=True)
    number_of_cabs = indexes.CharField(model_attr='number_of_cabs', indexed=False, null=True)
    number_of_floors = indexes.CharField(model_attr='number_of_floors', indexed=False, null=True)
    front = indexes.CharField(model_attr='front', indexed=False, null=True)
    rear = indexes.CharField(model_attr='rear', indexed=False, null=True)
    rgw = indexes.CharField(model_attr='rgw', indexed=False, null=True)
    capacity = indexes.CharField(model_attr='capacity', indexed=False, null=True)
    customer_po_number = indexes.CharField(model_attr='customer_po_number', indexed=False, null=True)
    delivery_date = indexes.CharField(model_attr='delivery_date', indexed=False, null=True)
    start_date = indexes.CharField(model_attr='start_date', indexed=False, null=True)
    estimated_price_for_job = indexes.CharField(model_attr='estimated_price_for_job', indexed=False, null=True)

    fixtures = indexes.IntegerField(model_attr='fixtures', indexed=False,  null=True)
    wood_shop = indexes.IntegerField(model_attr='wood_shop', indexed=False,  null=True)
    machine_shop = indexes.IntegerField(model_attr='machine_shop', indexed=False,  null=True)
    welding = indexes.IntegerField(model_attr='welding', indexed=False,  null=True)
    lacquer = indexes.IntegerField(model_attr='lacquer', indexed=False,  null=True)
    trim_shop = indexes.IntegerField(model_attr='trim_shop', indexed=False,  null=True)
    cab_assembly = indexes.IntegerField(model_attr='cab_assembly', indexed=False,  null=True)
    install_date = indexes.IntegerField(model_attr='install_date', indexed=False,  null=True)
    premier_glass = indexes.IntegerField(model_attr='premier_glass', indexed=False,  null=True)
    tile_installer = indexes.IntegerField(model_attr='tile_installer', indexed=False,  null=True)
    bill = indexes.IntegerField(model_attr='bill', indexed=False,  null=True)
    hayward = indexes.IntegerField(model_attr='hayward', indexed=False,  null=True)
    glenn = indexes.IntegerField(model_attr='glenn', indexed=False,  null=True)
    suren = indexes.IntegerField(model_attr='suren', indexed=False,  null=True)
    roger = indexes.IntegerField(model_attr='roger', indexed=False,  null=True)
    matt = indexes.IntegerField(model_attr='matt', indexed=False,  null=True)
    third_party_install = indexes.IntegerField(model_attr='third_party_install', indexed=False,  null=True)
    

    def get_model(self):
        return JobControl

    def prepare(self, obj):
        self.prepared_data = super(JobControlIndex, self).prepare(obj)
        sold_dict = {}
        try:
            sold_to_contact = obj.sold_to
            if sold_to_contact:
                # self.prepared_data['sold_to_id'] = sold_to_contact.id
                sold_dict['contact_name'] = sold_to_contact.contact_name
                sold_dict['attention_to'] = sold_to_contact.attention_to
                sold_dict['address_1'] = sold_to_contact.address_1
                sold_dict['address_2'] = sold_to_contact.address_2
                sold_dict['province'] = sold_to_contact.province
                sold_dict['city'] = sold_to_contact.city
                sold_dict['country'] = sold_to_contact.country
                sold_dict['post_code'] = sold_to_contact.postal_code
                sold_dict['id'] = sold_to_contact.id
                phones = ContactPhone.objects.filter(contact=sold_to_contact)
                phs = []
                for ph in phones:
                    phone = {}
                    phone['number'] = ph.phone
                    phone['ext'] = ph.phone_ext
                    phone['type'] = ph.phone_type.phone_type
                    phs.append(phone)

                sold_dict['phones'] = phs
                
                emails = ContactEmailAddress.objects.filter(contact=sold_to_contact)
                elist = []
                for em in emails:
                    email = {}
                    email['email_type'] = em.email_address_type.email_type
                    email['email_address'] = em.email_address
                    elist.append(email)

                sold_dict['emails'] = elist
            self.prepared_data['sold_to'] = sold_dict
        except Exception, e:
            self.prepared_data['sold_to'] = sold_dict
        

        # =============== Ship To =================================
        
        
        ship_dict = {}
        try:
            ship_to_contact = obj.ship_to
            if ship_to_contact:
                # self.prepared_data['sold_to_id'] = sold_to_contact.id
                ship_dict['contact_name'] = ship_to_contact.contact_name
                ship_dict['attention_to'] = ship_to_contact.attention_to
                ship_dict['address_1'] = ship_to_contact.address_1
                ship_dict['address_2'] = ship_to_contact.address_2
                ship_dict['province'] = ship_to_contact.province
                ship_dict['country'] = ship_to_contact.country
                ship_dict['post_code'] = ship_to_contact.postal_code
                ship_dict['city'] = ship_to_contact.city
                ship_dict['id'] = ship_to_contact.id
                phones = ContactPhone.objects.filter(contact=ship_to_contact)
                phs = []
                for ph in phones:
                    phone = {}
                    phone['number'] = ph.phone
                    phone['ext'] = ph.phone_ext
                    phone['type'] = ph.phone_type.phone_type
                    phs.append(phone)

                ship_dict['phones'] = phs
                
                emails = ContactEmailAddress.objects.filter(contact=ship_to_contact)
                elist = []
                for em in emails:
                    email = {}
                    email['email_type'] = em.email_address_type.email_type
                    email['email_address'] = em.email_address
                    elist.append(email)

                ship_dict['emails'] = elist
            self.prepared_data['ship_to'] = ship_dict
        except Exception, e:
            self.prepared_data['ship_to'] = ship_dict
            
        
        # ==================== Installed By =========================

        
        installed_by_dict = {}
        try:
            installed_by_contact = obj.installed_by
            if installed_by_contact:
                # self.prepared_data['sold_to_id'] = sold_to_contact.id
                installed_by_dict['contact_name'] = installed_by_contact.contact_name
                installed_by_dict['city'] = installed_by_contact.city
                installed_by_dict['id'] = installed_by_contact.id
                phones = ContactPhone.objects.filter(contact=installed_by_contact)
                phs = []
                for ph in phones:
                    phone = {}
                    phone['number'] = ph.phone
                    phone['ext'] = ph.phone_ext
                    phone['type'] = ph.phone_type.phone_type
                    phs.append(phone)

                installed_by_dict['phones'] = phs
                
                emails = ContactEmailAddress.objects.filter(contact=installed_by_contact)
                elist = []
                for em in emails:
                    email = {}
                    email['email_type'] = em.email_address_type.email_type
                    email['email_address'] = em.email_address
                    elist.append(email)

                installed_by_dict['emails'] = elist
            self.prepared_data['installed_by'] = installed_by_dict
        except Exception, e:
            self.prepared_data['installed_by'] = installed_by_dict
            
        
        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

class JobTimeControlIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    serial = indexes.CharField(model_attr='serial', null=True)
    job_number = indexes.CharField(model_attr='job_number', null=True)
    entry = indexes.DateTimeField(model_attr='entry', null=True)
    total_hours_worked = indexes.DecimalField(model_attr='total_hours_worked', null=True)
    eng_comment = indexes.CharField(model_attr='eng_comment', null=True)

    def get_model(self):
        return JobTimeControl

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare(self, obj):
        self.prepared_data = super(JobTimeControlIndex, self).prepare(obj)
        if obj.department_worked:
            self.prepared_data['department_worked'] = obj.department_worked.production_type_name
            self.prepared_data['department_worked_id'] = obj.department_worked.id
        else:
            self.prepared_data['department_worked'] = None


        if obj.job_number:
            sold_to = {}
            if obj.job_number.sold_to:
                sold_to['contact_name'] = obj.job_number.sold_to.contact_name
                sold_to['attention_to'] = obj.job_number.sold_to.attention_to

            job_control = {}
            
            job_control['job_number'] = obj.job_number.job_number
            job_control['job_name'] = obj.job_number.job_name
            job_control['sold_to'] = sold_to

            self.prepared_data['job_control'] = job_control

        return self.prepared_data
