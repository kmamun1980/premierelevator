STOCK_STATUS_TYPE = (
	(0, 'Normal tracking item'),
	(1, 'Non-stock item'),
	(2, 'Labour for shop specified'),
	(3, 'NC labour'),
	(4, 'Obsolete'),
	(5, 'Production Hold'),
)

PO_STATUS_CHOICES = (
	('new', 'New'),
	('in-progress', 'In progress- Awating Delivery'),
	('received-partial', 'Received Partial'),
	('on-hold', 'On Hold'),
	('closed', 'Closed'),

)

SHOP_STATUS_CHOICES = (
	('new', 'New'),
	('in-progress', 'In Progress'),
	('on-hold', 'On Hold'),
	('completed', 'Completed'),
)