import datetime
from haystack import indexes
from inventory.models import Item
from contacts.models import ContactPhone, ContactEmailAddress
import uuid

class ItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    item_number = indexes.CharField(model_attr='item_number')
    description = indexes.CharField(model_attr='description')
    quantity_on_hand = indexes.DecimalField(model_attr='quantity_on_hand', indexed=False, default=0.0)
    quantity_on_order = indexes.DecimalField(model_attr='quantity_on_order', indexed=False, default=0.0)
    qty_received = indexes.DecimalField(model_attr='qty_received', indexed=False, default=0.0)
    qty_received_date = indexes.DateField(model_attr='qty_received_date', indexed=False, null=True)
    last_PO = indexes.CharField(model_attr='last_PO', indexed=False, null=True)
    last_PO_date_ordered = indexes.DateField(model_attr='last_PO_date_ordered', indexed=False, null=True)
    last_PO_date_expected = indexes.DateField(model_attr='last_PO_date_expected', indexed=False, null=True)
    last_cost_paid = indexes.DecimalField(model_attr='last_cost_paid', indexed=False, default=0.0)
    comments = indexes.CharField(model_attr='comments', indexed=False, null=True)
    warehouse_location = indexes.CharField(model_attr='warehouse_location', indexed=False, null=True)
    lowest_price_paid = indexes.DecimalField(model_attr='lowest_price_paid', indexed=False, default=0.0)
    lowest_price_last_buy_date = indexes.DateField(model_attr='lowest_price_last_buy_date', indexed=False, null=True)
    qty_on_request = indexes.DecimalField(model_attr='qty_on_request', indexed=False, default=0.0)
    max_order_qty = indexes.DecimalField(model_attr='max_order_qty', indexed=False, default=0.0)
    max_single_order_qty = indexes.DecimalField(model_attr='max_single_order_qty', indexed=False, default=0.0)
    max_order_qty_remains = indexes.DecimalField(model_attr='max_order_qty_remains', indexed=False, default=0.0)
    wholesale_cost = indexes.DecimalField(model_attr='wholesale_cost', indexed=False, default=0.0)
    retail_price = indexes.DecimalField(model_attr='retail_price', indexed=False, default=0.0)
    estimated_wholesale_cost = indexes.DecimalField(model_attr='estimated_wholesale_cost', indexed=False, default=0.0)
    catalog_number = indexes.CharField(model_attr='catalog_number', indexed=False, null=True)
    country_of_origin = indexes.CharField(model_attr='country_of_origin', indexed=False, null=True)
    lead_time = indexes.CharField(model_attr='lead_time', indexed=False, null=True)
    customer_tariff_number = indexes.CharField(model_attr='customer_tariff_number', indexed=False, null=True)
    preference_criteria = indexes.CharField(model_attr='preference_criteria', indexed=False, null=True)
    shipping_unit = indexes.CharField(model_attr='shipping_unit', indexed=False, null=True)
    shipping_weight = indexes.DecimalField(model_attr='shipping_weight', indexed=False, default=0.0)
    minimum_qty_on_hand = indexes.DecimalField(model_attr='minimum_qty_on_hand', indexed=False, default=0.0)
    duty_percentage = indexes.DecimalField(model_attr='duty_percentage', indexed=False, null=True)
    hst_taxable = indexes.BooleanField(model_attr='hst_taxable', indexed=False, null=True)
    pst_taxable = indexes.BooleanField(model_attr='pst_taxable', indexed=False, null=True)
    date_added = indexes.DateTimeField(model_attr='date_added', indexed=False, null=True)
    date_modified = indexes.DateTimeField(model_attr='date_modified', indexed=False, null=True)
    website = indexes.CharField(model_attr='website', indexed=False, null=True)
    order_restriction = indexes.DecimalField(model_attr='order_restriction', indexed=False, default=0.0)
    
    def get_model(self):
        return Item


    def prepare(self, obj):
        self.prepared_data = super(ItemIndex, self).prepare(obj)
        
        if obj.item_number == '.':
            x = uuid.uuid4()
            self.prepared_data['unique_number'] = str(x)
        else:
            self.prepared_data['unique_number'] = None

        if obj.department:
            dep = {}
            dep['id'] = obj.department.id
            dep['name'] = obj.department.name
            self.prepared_data['department'] = dep
        else:
            self.prepared_data['department'] = None
        
        if obj.primary_supplier:
            supplier = obj.primary_supplier
            supplier_doc = {}

            try:
                supplier_profile = obj.primary_supplier.contactprofile
                if supplier_profile.fob:
                    supplier_doc['fob'] = supplier_profile.fob
                if supplier_profile.terms:
                    supplier_doc['terms'] = supplier_profile.terms.term
                    supplier_doc['term_id'] = supplier_profile.terms.id
                if supplier_profile.shipping_method:
                    supplier_doc['shipvia'] = supplier_profile.shipping_method.delivery_choice
                    supplier_doc['shipvia_id'] = supplier_profile.shipping_method.id

                supplier_doc['hst_tax'] = supplier_profile.hst_tax_exempt
                supplier_doc['pst_tax'] = supplier_profile.pst_tax_exempt

            except Exception, e:
                pass
        
            supplier_doc['contact_name'] = supplier.contact_name
        
            supplier_doc['id'] = supplier.pk
            supplier_doc['address_1'] = supplier.address_1
            supplier_doc['attention_to'] = supplier.attention_to
            supplier_doc['city'] = supplier.city
            supplier_doc['province'] = supplier.province
            supplier_doc['country'] = supplier.country
            supplier_doc['postal_code'] = supplier.postal_code

            supplier_doc['phones'] = []
            phones = ContactPhone.objects.filter(contact=supplier)
            for cphone in phones:
                phone = {}
                phone['type'] = cphone.phone_type.phone_type
                phone['number'] = cphone.phone
                phone['phone_ext'] = cphone.phone_ext
                supplier_doc['phones'].append(phone)

            supplier_doc['emails'] = []
            cemails = ContactEmailAddress.objects.filter(contact=supplier)

            for cemail in cemails:
                email = {}
                email['email_address_type'] = cemail.email_address_type.email_type
                email['email_address'] = cemail.email_address
                supplier_doc['emails'].append(email)

            self.prepared_data['supplier'] = supplier_doc
        else:
            self.prepared_data['supplier'] = None

        if obj.last_PO_ordered_by:
            order_by = {}
            order_by['id'] = obj.last_PO_ordered_by.id
            order_by['username'] = obj.last_PO_ordered_by.username
            order_by['email'] = obj.last_PO_ordered_by.email
            self.prepared_data['last_PO_ordered_by'] = order_by
        else:
            self.prepared_data['last_PO_ordered_by'] = None

        if obj.last_PO_supplier:
            last_supplier = obj.last_PO_supplier
            supplier_dict = {}
            supplier_dict['id'] = last_supplier.id
            supplier_dict['contact_name'] = last_supplier.contact_name
            supplier_dict['attention_to'] = last_supplier.attention_to
            supplier_dict['address_1'] = last_supplier.address_1
            supplier_dict['address_2'] = last_supplier.address_2
            supplier_dict['city'] = last_supplier.city
            supplier_dict['province'] = last_supplier.province
            supplier_dict['country'] = last_supplier.country
            supplier_dict['postal_code'] = last_supplier.postal_code
            phones = last_supplier.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            supplier_dict['phones'] = phs
            emails = last_supplier.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            self.prepared_data['last_PO_supplier'] = supplier_dict
        else:
            self.prepared_data['last_PO_supplier'] = None

        if obj.item_unit_measure:
            unit = {}
            unit['id'] = obj.item_unit_measure.id
            unit['unit_name'] = obj.item_unit_measure.unit_name
            self.prepared_data['unit'] = unit
        else:
            self.prepared_data['unit'] = None

        if obj.stock_status_type:
            stock_status = {}
            stock_status['id'] = obj.stock_status_type
            stock_status['status'] = obj.get_stock_status
            self.prepared_data['stock_status_type'] = stock_status
        else:
            self.prepared_data['stock_status_type'] = None
        
        if obj.currency:
            currency = {}
            currency['id'] = obj.currency.id
            currency['currency'] = obj.currency.currency
            self.prepared_data['currency'] = currency
        else:
            self.prepared_data['currency'] = None

        if obj.production_type:
            prod_type = {}
            prod_type['id'] = obj.production_type.id
            prod_type['production_type_name'] = obj.production_type.production_type_name
            prod_type['cost_per_hour'] = obj.production_type.cost_per_hour            
            self.prepared_data['production_type'] = prod_type
        else:
            self.prepared_data['production_type'] = None

        if obj.customs_designation:
            designation = {}
            designation['id'] = obj.customs_designation.id
            designation['designation'] = obj.customs_designation.designation
            self.prepared_data['customs_designation'] = designation
        else:
            self.prepared_data['customs_designation'] = None

        if obj.producer_of_item:
            last_supplier = obj.producer_of_item
            supplier_dict = {}
            supplier_dict['id'] = last_supplier.id
            supplier_dict['contact_name'] = last_supplier.contact_name
            supplier_dict['attention_to'] = last_supplier.attention_to
            supplier_dict['address_1'] = last_supplier.address_1
            supplier_dict['address_2'] = last_supplier.address_2
            supplier_dict['city'] = last_supplier.city
            supplier_dict['province'] = last_supplier.province
            supplier_dict['country'] = last_supplier.country
            supplier_dict['postal_code'] = last_supplier.postal_code
            phones = last_supplier.contact_phone.all()
            phs = []
            for ph in phones:
                phone = {}
                phone['number'] = ph.phone
                phone['ext'] = ph.phone_ext
                phone['type'] = ph.phone_type.phone_type
                phs.append(phone)
            supplier_dict['phones'] = phs
            emails = last_supplier.contact_emails.all()
            elist = []
            for em in emails:
                email = {}
                email['email_type'] = em.email_address_type.email_type
                email['email_address'] = em.email_address
                elist.append(email)

            self.prepared_data['producer_of_item'] = supplier_dict
        else:
            self.prepared_data['producer_of_item'] = None

        if obj.terms:
            term = {}
            term['id'] = obj.terms.id
            term['term'] = obj.terms.term
            self.prepared_data['terms'] = term
        else:
            self.prepared_data['terms'] = None

        if obj.deliver_internal:
            deliver_internal_dict = {}
            deliver_internal_dict['id'] = obj.deliver_internal.id
            deliver_internal_dict['department'] = obj.deliver_internal.department
            deliver_internal_dict['description'] = obj.deliver_internal.description            
            self.prepared_data['deliver_internal'] = deliver_internal_dict
        else:
            self.prepared_data['deliver_internal'] = None

        return self.prepared_data


    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    # def prepare_department(self, obj):
    #     if obj.department:
    #         return obj.department.name
    #     else:
    #         return 'None'


    