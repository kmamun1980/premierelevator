from django import forms
from django.contrib import admin
from inventory.models import *
from inventory.lookups import *
from inventory.choices import *
from contacts.models import *
import selectable
from django.db import models
from django.utils.safestring import mark_safe
from premierelevator.models import SystemVariable
from django.forms import Widget, TextInput
import re
import datetime


class PremierTextInput(TextInput):
	"""docstring for PremierTextInput"""
	# def __init__(self, arg):
	# 	super(PremierTextInput, self).__init__()
	
	def render(self, name, value, attrs=None):
		str1 = TextInput.render(self, name, value, attrs)
		str2 = '<a href="/admin/contacts/contact/add/" class="add-another" id="add_id_primary_supplier" onclick="return showAddAnotherPopup(this);"> <img src="/statics/admin/img/icon_addlink.gif" width="10" height="10" alt="Add Another"></a>'
		output = str1 + str2
		return mark_safe(output)

		

class ItemUnitMeasureForm(forms.ModelForm):
	class Meta:
		model = ItemUnitMeasure
		exclude = ['is_active',]


class ProductionTypeForm(forms.ModelForm):
	class Meta:
		model = ProductionType
		exclude = ['is_active',]


class CustomsDesignationForm(forms.ModelForm):
	designation = forms.CharField( max_length=25,
        widget=forms.TextInput(attrs={"class": "form-control", 'required': 'True', 'placeholder': "Custom Designation"}))

	class Meta:
		model = CustomsDesignation
		exclude = ['is_active',]


class ItemForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request', None)
		self.action = kwargs.pop('action', None)
		super(ItemForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Item
		# fields = ('item_number', 'description', 'currency', 'wholesale_cost', 'quantity_on_hand', 
		# 	'quantity_on_order', 'qty_on_request', 'department', 'primary_supplier', 
		# 	'item_unit_measure', 'warehouse_location', 'max_order_qty', 
		# 	'max_single_order_qty', 'estimated_wholesale_cost', 'retail_price', 'production_type',
		# 	'catalog_number', 'country_of_origin', 'lead_time', 'customs_designation', 
		# 	'customer_tariff_number', 'preference_criteria', 'producer_of_item', 'shipping_weight', 'shipping_unit',
		# 	'minimum_qty_on_hand', 'deliver_internal', 'duty_percentage', 'terms', 'website', 'item_image')

	def save(self, commit=True):
		item = super(ItemForm, self).save(commit=False)
		if self.action == 'new':
			item.max_order_qty_remains = item.max_order_qty
			# item.quantity_on_hand = 0.0;
			# item.quantity_on_order = 0.0;
			# item.qty_received = 0.0;
			# item.qty_on_request = 0.0;
			item.date_added = datetime.datetime.now();
		
		item.save()
		return item


class ItemCommentForm(forms.ModelForm):
	comment = forms.CharField( max_length=250, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", 'rows':2, 'placeholder':"Item Comment"}))
	
	class Meta:
		model = ItemComment
		exclude = ['item', 'comment_date', 'comment_by']


class ItemLookupForm(forms.Form):
	autocomplete = forms.CharField(
          label='Type for auto suggession',
          widget=selectable.forms.AutoCompleteWidget(ContactLookup, 
          attrs={"class": "form-control", 'placeholder': "Filter item but item number, department and product type", 'name': 'item-autocomplete-filter'}),
          required=False,

      )



class LocationForm(forms.ModelForm):
	warehouse_location = forms.CharField( max_length=50,
        widget=forms.TextInput(attrs={"class": "form-control", 'required': 'True', 'placeholder':"Location"}))
	description = forms.CharField( max_length=250, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", 'rows':2, 'required': 'False', 'placeholder':"Location Description"}))
	
	class Meta:
		model = Location	






class DeliverInternalForm(forms.ModelForm):
    department = forms.CharField( max_length=50,
        widget=forms.TextInput(attrs={"class": "form-control", 'required': 'True', 'placeholder':"Department"}))
    description = forms.CharField( max_length=250, required=False,
        widget=forms.Textarea(attrs={"class": "form-control", 'rows':2, 'required': 'False', 'placeholder':"Department Description"}))
    
    class Meta:
        model = DeliverInternal 




