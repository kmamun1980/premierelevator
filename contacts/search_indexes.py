import datetime
from haystack import indexes
from contacts.models import Contact, ContactPhone, ContactEmailAddress


class ContactIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    contact_name = indexes.CharField(model_attr='contact_name', null=True)
    attention_to = indexes.CharField(model_attr='attention_to', indexed=False, null=True)
    address_1 = indexes.CharField(model_attr='address_1', null=True)
    address_2 = indexes.CharField(model_attr='address_2', null=True)
    city = indexes.CharField(model_attr='city', indexed=False, null=True)
    province = indexes.CharField(model_attr='province', indexed=False, null=True)
    country = indexes.CharField(model_attr='country', indexed=False, null=True)
    postal_code = indexes.CharField(model_attr='postal_code', indexed=False, null=True)
    # phones = indexes.MultiValueField(indexed=False, null=True)
    # search_string = indexes.NgramField(model_attr='search_string')

    def get_model(self):
        return Contact

    def prepare(self, obj):
        self.prepared_data = super(ContactIndex, self).prepare(obj)
        # phones = obj.contact_phone.all()
        phones = ContactPhone.objects.filter(contact=obj)
        phs = []
        for ph in phones:
            phone = {}
            phone['id'] = ph.id
            if ph.phone_type:
                phone['department'] = ph.phone_type.phone_type
            else:
                phone['department'] = 'Main'

            phone['number'] = ph.phone
            phone['ext'] = ph.phone_ext
            phone['contact_name'] = obj.attention_to
            phs.append(phone)
        self.prepared_data['phones'] = phs
        # emails = obj.contact_emails.all()
        emails = ContactEmailAddress.objects.filter(contact=obj)
        elist = []
        for em in emails:
            email = {}
            email['id'] = em.id
            if em.email_address_type:
                email['department'] = em.email_address_type.email_type
            else:
                email['department'] = 'Personal'
            email['email'] = em.email_address
            email['contact_name'] = obj.attention_to
            elist.append(email)

        self.prepared_data['emails'] = elist

        self.prepared_data['faxes'] = []
        
        try:
            con_profile = obj.contactprofile
            self.prepared_data['hst_tax_exempt'] = con_profile.hst_tax_exempt
            self.prepared_data['hst_number'] = con_profile.hst_number
            self.prepared_data['pst_tax_exempt'] = con_profile.pst_tax_exempt
            self.prepared_data['pst_number'] = con_profile.pst_number
            if con_profile.terms:
                term = {}
                term['id'] = obj.terms.id
                term['term'] = obj.terms.term
                self.prepared_data['terms'] = term
            else:
                self.prepared_data['terms'] = None

            if con_profile.shipping_method:
                ship = {}
                ship['id'] = con_profile.shipping_method.id
                ship['delivery_choice'] = con_profile.delivery_choice
                self.prepared_data['shipping_method'] = ship
            else:
                self.prepared_data['shipping_method'] = None

        except Exception, e:
            pass

        return self.prepared_data

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()